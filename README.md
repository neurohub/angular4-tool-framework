# Drag & Drop with Dynamic Components

```
app:{
	editor:{
		tool-palette: [
			tool1,
			tool2,
			...
		]
		work-area: [
			tool1,
			tool2,
			...
		]
	}
}
```

The Editor Components creates the list of tools available in the tool-palette

All the tools can be dragged to the work-area

The template of the tool should depend on the parent Component where is dropped 


