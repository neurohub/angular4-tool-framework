import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[tool]'
})
export class ToolDirective {

    constructor(public viewContainerRef: ViewContainerRef) {
    }

}
