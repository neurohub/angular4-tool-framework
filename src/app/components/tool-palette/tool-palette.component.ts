import {Component} from '@angular/core';
import {AbstractDraggableComponent} from "../abstract-draggable/abstract-draggable.component";
import {DropEffect} from "ngx-drag-drop";

@Component({
    selector: 'app-tool-palette',
    template: `
        <div>
            <pre>Tool Palette</pre>
            <ul class="list-group">
                <li class="list-group-tool" *ngFor="let tool of tools"
                    [dndDraggable]="tool"
                    [dndEffectAllowed]="tool.effectAllowed"
                    [dndDisableIf]="tool.disable"
                    (dndStart)="onDragStart($event)"
                    (dndCopied)="onDragged(tool, tools, 'copy')"
                    (dndLinked)="onDragged(tool, tools, 'link')"
                    (dndMoved)="onDragged(tool, tools, 'move')"
                    (dndCanceled)="onDragged(tool, tools, 'none')"
                    (dndEnd)="onDragEnd($event)">
                    <app-tool [tool]="tool.component"></app-tool>
                </li>
            </ul>
        </div>
    `
})
export class ToolPaletteComponent extends AbstractDraggableComponent {
    onDragged(tool: any, list: any[], effect: DropEffect) {
    }
}
