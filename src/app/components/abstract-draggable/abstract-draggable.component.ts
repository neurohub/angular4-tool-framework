import {Component, Input} from "@angular/core";
import {ToolItem} from "../tool/tool-item";
import {DropEffect, DndDropEvent} from "ngx-drag-drop";

@Component({
    template: ''
})
export class AbstractDraggableComponent {
    @Input() tools: ToolItem[];
    private currentDraggableEvent: DragEvent;

    onDragStart(event: DragEvent) {
        console.log('onDragStart', event);
        this.currentDraggableEvent = event;
    }

    onDragged(item: any, list: any[], effect: DropEffect) {
        console.log('onDragged', item, list);
        if (effect === "move") {
            const index = list.indexOf(item);
            list.splice(index, 1);
        }
    }

    onDragEnd(event: DragEvent) {
        console.log('onDragEnd', event);
        this.currentDraggableEvent = event;
    }

    onDrop(event: DndDropEvent, list?: any[]) {
        console.log('onDrop', event, list);
        if (list
            && (event.dropEffect === "copy"
            || event.dropEffect === "move")) {

            let index = event.index;

            if (typeof index === "undefined") {

                index = list.length;
            }

            list.splice(index, 0, event.data);
        }
    }
}
