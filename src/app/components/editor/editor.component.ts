import {Component} from '@angular/core';
import {ToolItem} from "../tool/tool-item";
import {TextAreaComponent} from "../tools/text-area/text-area.component";
import {ButtonComponent} from "../tools/button/button.component";

@Component({
    selector: 'app-editor',
    template: `
        <app-tool-palette [tools]="tools"></app-tool-palette>
        <app-work-area ></app-work-area>
        <app-trash [tools]="[]"></app-trash>
    `
})
export class EditorComponent {
    public tools: ToolItem[];

    constructor() {
        this.tools = [
            new ToolItem(TextAreaComponent),
            new ToolItem(ButtonComponent),
            new ToolItem(TextAreaComponent)
        ];
    }

}
