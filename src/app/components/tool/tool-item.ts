import {Type} from '@angular/core';

export class ToolItem {

    public  data;
    public  effectAllowed;
    public  disable;
    public  handle;

    constructor(public component: Type<any>) {
        this.data= {};
        this.effectAllowed= "copyMove";
        this.disable= false;
        this.handle= false;
    }
}
