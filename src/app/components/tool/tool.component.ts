import {Component, OnInit, Input, ViewChild, ComponentFactoryResolver, Type} from '@angular/core';
import {ToolDirective} from "../../directives/tool.directive";

@Component({
    selector: 'app-tool',
    template: `<ng-template tool></ng-template>`
})
export class ToolComponent implements OnInit {

    @Input() tool: Type<ToolComponentInterface>;
    @ViewChild(ToolDirective) directive: ToolDirective;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    ngOnInit() {
        console.log(this.tool);
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tool);
        let viewContainerRef = this.directive.viewContainerRef;
        viewContainerRef.clear();
        viewContainerRef.createComponent(componentFactory);
    }

}

export interface ToolComponentInterface {
    component: Type<any>;
}
