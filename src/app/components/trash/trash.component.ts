import {Component} from '@angular/core';
import {AbstractDraggableComponent} from "../abstract-draggable/abstract-draggable.component";

@Component({
  selector: 'app-trash',
  template: `
        <div (dndDrop)="onDrop($event, tools)"
                dndDropzone
                dndEffectAllowed="copyMove"
                class="dndList">
                <pre>Trash</pre>
        </div>
    `
})
export class TrashComponent extends AbstractDraggableComponent  {

}
