import {Component} from '@angular/core';
import {AbstractDraggableComponent} from "../abstract-draggable/abstract-draggable.component";

@Component({
    selector: 'app-work-area',
    template: `
        <div (dndDrop)="onDrop($event, tools)"
                dndDropzone
                dndEffectAllowed="copyMove"
                class="dndList">
            <pre>Work Area</pre>
            <ul class="list-group">
                <li class="list-group-tool" *ngFor="let tool of tools"
                    [dndDraggable]="tool"
                    [dndEffectAllowed]="tool.effectAllowed"
                    [dndDisableIf]="tool.disable"
                    (dndStart)="onDragStart($event)"
                    (dndCopied)="onDragged(tool, tools, 'copy')"
                    (dndLinked)="onDragged(tool, tools, 'link')"
                    (dndMoved)="onDragged(tool, tools, 'move')"
                    (dndCanceled)="onDragged(tool, tools, 'none')"
                    (dndEnd)="onDragEnd($event)">
                    <app-tool [tool]="tool.component"></app-tool>
                </li>
            </ul>
        </div>
    `
})
export class WorkAreaComponent extends AbstractDraggableComponent  {

}
