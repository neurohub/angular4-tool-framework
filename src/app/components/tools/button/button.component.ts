import {Component, Input, Type} from '@angular/core';
import {ToolComponentInterface} from "../../tool/tool.component";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements ToolComponentInterface {
  @Input() component: Type<any>
}
