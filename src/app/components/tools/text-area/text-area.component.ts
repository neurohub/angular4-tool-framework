import {Component, Input, Type} from '@angular/core';
import {ToolComponentInterface} from "../../tool/tool.component";

@Component({
    selector: 'app-text-area',
    templateUrl: './text-area.component.html',
    styleUrls: ['./text-area.component.css']
})
export class TextAreaComponent implements ToolComponentInterface {
    @Input() component: Type<any>;
    @Input() data: any;
    @Input() effectAllowed: string;
    @Input() disable: boolean;
    @Input() handle: boolean;
}
