import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FlexLayoutModule} from "@angular/flex-layout";
import {HttpModule} from "@angular/http";
import {AppComponent} from './app.component';
import {ToolPaletteComponent} from './components/tool-palette/tool-palette.component';
import {ToolComponent, ToolComponentInterface} from './components/tool/tool.component';
import {WorkAreaComponent} from './components/work-area/work-area.component';
import {EditorComponent} from './components/editor/editor.component';
import {AlertModule} from 'ngx-bootstrap';
import {DndModule} from 'ngx-drag-drop';
import {MatSnackBarModule} from "@angular/material";
import {TextAreaComponent} from './components/tools/text-area/text-area.component';
import {ToolDirective} from './directives/tool.directive';
import { ButtonComponent } from './components/tools/button/button.component';
import { AbstractDraggableComponent } from './components/abstract-draggable/abstract-draggable.component';
import { TrashComponent } from './components/trash/trash.component';

@NgModule({
    declarations: [
        AppComponent,
        ToolPaletteComponent,
        WorkAreaComponent,
        EditorComponent,
        TextAreaComponent,
        ToolDirective,
        ToolComponent,
        ButtonComponent,
        AbstractDraggableComponent,
        TrashComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        DndModule,
        FlexLayoutModule,
        HttpModule,
        MatSnackBarModule,
        AlertModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [WorkAreaComponent, ToolPaletteComponent, AbstractDraggableComponent,ToolComponent, TextAreaComponent, ButtonComponent],

})
export class AppModule {
}
